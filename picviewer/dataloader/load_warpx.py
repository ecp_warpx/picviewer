import numpy as np

import yt
from yt.funcs import mylog
mylog.setLevel(0)

class LoadWarpx:

    def __init__(self, plotfile_prefix):
        self.plotfile_prefix = plotfile_prefix

    def loadfield(self, 
                filepath,
                dim,
                dxfact,
                dyfact,
                dzfact,
                iteration, 
                field,
                amrlevel):

        fname =  filepath + '/' + self.plotfile_prefix + str('%5.5d'%(iteration))
        ds = yt.load(fname)

        dim_factor = 2**amrlevel
        #print(amrlevel, dim_factor)

        #if dim == 3:
        #    nx, ny, nz = ds.domain_dimensions
        #else:
        #    nx, nz, ny = ds.domain_dimensions

        all_data_level = ds.covering_grid(level=amrlevel,
            left_edge=ds.domain_left_edge, dims=dim_factor*ds.domain_dimensions)
        if dim == 3:
            tempdata = all_data_level['boxlib', field][::dxfact, ::dyfact, ::dzfact].d
        else:
            tempdata = all_data_level['boxlib', field][::dxfact, ::dzfact, 0].d

        tempdata = np.float32(tempdata)

        return tempdata

    def loadparticle(self,
                filepath,
                dim,
                dpfact,
                iteration,
                species,
                variable):

        C = 2.99792458e8 # light speed

        fname =  filepath + '/' + self.plotfile_prefix + str('%5.5d'%(iteration))
        ds = yt.load(fname)
        ad = ds.all_data()

        numpart = ds.particle_type_counts[species]

        def get_tempdata(quantity):
            try:
                tempdata = ad[(species,quantity)][::dpfact[species]].d
            except yt.utilities.exceptions.YTFieldNotFound:
                tempdata = []
            return np.float32(tempdata)

        if variable in ['t']:
            tempdata = get_tempdata('particle_theta')
    
        if variable in ['x','y','z']:
            if dim == 2:
                if variable == 'z':
                    tempdata = get_tempdata('particle_position_y')
                else:
                    tempdata = get_tempdata('particle_position_'+variable)
            else:
                tempdata = get_tempdata('particle_position_'+variable)
            
            tempdata = tempdata*1e6

        if variable in ['px','py','pz']:    
            tempdata = get_tempdata('particle_momentum_'+variable[1])

            tempdata = tempdata/C

        if variable in ['w']:
            tempdata = get_tempdata('particle_weight')

        return tempdata

    def getAMRboundaries(self,
                filepath,
                dim,
                sliceplane,
                iteration,
                amrlevel):

        fname =  filepath + '/' + self.plotfile_prefix + str('%5.5d'%(iteration))
        ds = yt.load(fname)

        xleftedge=[]
        xrightedge=[]
        zleftedge=[]
        zrightedge=[]

        
        if dim == 2:
            for grids in ds.index.grids:
                if grids.Level == amrlevel:
                    xleftedge.append(grids.LeftEdge[0].d*1.e6)
                    xrightedge.append(grids.RightEdge[0].d*1.e6)
                    zleftedge.append(grids.LeftEdge[1].d*1.e6)
                    zrightedge.append(grids.RightEdge[1].d*1.e6)

            x1leftedge = np.min(xleftedge)
            x1rightedge = np.max(xrightedge)
            x2leftedge = np.min(zleftedge)
            x2rightedge = np.max(zrightedge)
                
        else:

            yleftedge=[]
            yrightedge=[]
            for grids in ds.index.grids:
                if grids.Level == amrlevel:
                    xleftedge.append(grids.LeftEdge[0].d*1.e6)
                    xrightedge.append(grids.RightEdge[0].d*1.e6)
                    yleftedge.append(grids.LeftEdge[1].d*1.e6)
                    yrightedge.append(grids.RightEdge[1].d*1.e6)
                    zleftedge.append(grids.LeftEdge[2].d*1.e6)
                    zrightedge.append(grids.RightEdge[2].d*1.e6)
                        
            if sliceplane == 'yx':
                x1leftedge = np.min(xleftedge)
                x1rightedge = np.max(xrightedge)
                x2leftedge = np.min(yleftedge)
                x2rightedge = np.max(yrightedge)
            if sliceplane == 'xz':
                x1leftedge = np.min(zleftedge)
                x1rightedge = np.max(zrightedge)
                x2leftedge = np.min(xleftedge)
                x2rightedge = np.max(xrightedge)
            if sliceplane == 'yz':
                x1leftedge = np.min(zleftedge)
                x1rightedge = np.max(zrightedge)
                x2leftedge = np.min(yleftedge)
                x2rightedge = np.max(yrightedge)
                
            
        return x1leftedge, x1rightedge, x2leftedge, x2rightedge
